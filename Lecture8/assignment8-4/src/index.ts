//8-4
import express, { Request, Response } from "express"

const server = express()

server.listen(3001, () => {
	console.log("Listening to port 3001")
})

const nameCounters: Array<{name: string, times: number}> = [] // interface käyttöön

server.get("/counter/:name", (req: Request, res: Response) => {
	const name = req.params.name
	let info = nameCounters.find(item => item.name === name) // jos lista on nameCounters, sen sisällä on loogisesti nameCounter
    // const nameCounter = nameCounters.find(nameCounter => nameCounter.name === name)

	if (info === undefined) {
		info = {name: name, times: 1}
		nameCounters.push(info)
	} else {
		info.times += 1
	}
	
	res.send(`${name} was here ${info.times} times`)
})

