//8-7

import express, { Request, Response} from "express"
import { logger, unknownEndpoint } from "./middlewares"

const server = express()
server.use(express.json())

server.use(logger)

server.get("/students", (req: Request, res: Response) => {
	res.send([])
})

server.use(unknownEndpoint)

server.listen(3002)


