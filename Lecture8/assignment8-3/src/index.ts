//8-3
import express, { Request, Response } from "express"

const server = express()
let times = 0

server.listen(3001, () => {
	console.log("Listening to port 3001")
})

server.get("/counter", (req: Request, res: Response) => {
	times += 1
	if (req.query.times) {
		times = Number(req.query.times)
		console.log(req.query.times)
	}
	
	res.send(String(times))
})

