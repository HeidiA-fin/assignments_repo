//8-9

import express, { Request, Response } from "express"
import { logger, unknownEndpoint } from "./middlewares"

const server = express()
server.use(express.json())

const students: Array<{ id: number, name: string, email: string }> = []

server.use(logger)

server.get("/students", (req: Request, res: Response) => {
    res.send(students.map((student) => student.id))
})

server.post("/student", (req: Request, res: Response) => {

    const student = req.body
    if (typeof student.id !== "number" || typeof student.name !== "string" || typeof student.email !== "string") {
        res.status(400).send("ERROR, missing parameter or wrong type")
    } else {
        const studentInfo = { id: student.id, name: student.name, email: student.email }
        students.push(studentInfo)
        res.status(201).send()
    }
})

//i dont know if this PUT works the right way tho
server.put("/student/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, email } = req.body
    // hyvin sulla on käytössä asiat joista just edelliseen oon kirjottanu kommentteja :D
    const student = students.find((item) => item.id === id) // tääl silti item vois olla student

    if (student === undefined) {
        return res.status(404).send("ERROR, student id not found") // jos laitat returnit erroriin, niin "oikea" käyttäytyminen ei ole else-haaran sisällä

    } else if (!name && !email) {
        return res.status(400).send("ERROR, name or email must be provided")

    }

    if (name) {
        student.name = name
    }
    if (email) {
        student.email = email
    }
    res.status(204).send()
})
// PUT is looking good. They're always a bit tricky, since there's so many possible ways to go. 

server.delete("/student/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const index = students.findIndex((item) => item.id === id)

    if (index === -1) {
        res.status(404).send("ERROR, student id not found")
    } else {
        students.splice(index, 1)
        res.status(204).send()
    }
    // Suosittelen tähän immutaabelia filter-metodia
    // students = students.filter(student => student.id !== id)
})

server.get("/student/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const info = students.find(item => item.id === id) // nyt se on välillä info ja välillä student
    if (info === undefined) {
        res.status(404).send("ERROR, student id not found")
    } else {
        res.send(info)
    }
})

server.use(unknownEndpoint)
server.listen(3002)