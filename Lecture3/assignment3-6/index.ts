
/* 1 START hox this is a bad way to do these
let num = 0
for (let i = 0; i <= 900; i++) {
    num = i + 100;
    if (i % 100) {
        i + 100
} else console.log(num)

}
// ^ tää on aika erikoinen tapa tehdä tämä

1 END*/

/*2 START hox this is a better way to do these
for (let i = 1; i <= 128; i *= 2) {
    console.log(i)
}
2 END*/

/*3 START
for (let i = 3; i <= 15; i += 3) {
    console.log(i)
}
3 END*/

/*4 START
for (let i = 9; i >= 0; i -= 1) {
    console.log(i)
}
4 END*/

/*5 START
for (let i = 1; i <= 4; i += 1) {
    for (let b = 0; b < 3; b++) {
    console.log(i)
    }
}
5 END*/

/* 6 START
for (let i = 1; i <= 3; i += 1) {
    for (let b = 0; b < 5; b++) {
    console.log(b)
    }
}
6 END */ 

// nää muut on paljon loogisempia