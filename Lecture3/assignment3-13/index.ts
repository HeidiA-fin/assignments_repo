const month = Number(process.argv[2])

switch (month) {
   case 1:
      console.log("There is 31 days in January");
      break;
   case 2:
      console.log("There is 28 days in February");
      break;
   case 3:
      console.log("There is 31 days in March");
      break;
   case 4:
      console.log("There is 30 days in April");
      break;
   case 5:
      console.log("There is 31 days in May");
      break;
   case 6:
      console.log("There is 30 days in June");
      break;
   case 7:
      console.log("There is 31 days in July");
      break;
   case 8:
      console.log("There is 31 days in August");
      break;
   case 9:
      console.log("There is 30 days in September");
      break;
   case 10:
      console.log("There is 31 days in October");
      break;
   case 11:
      console.log("There is 30 days in November");
      break;
   case 12:
      console.log("There is 31 days in December");
      break;
   default:
      console.log("What are you doing, again??");
      break;
}