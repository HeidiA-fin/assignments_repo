const num1 = Number(process.argv[2])
const num2 = Number(process.argv[3])
const password = process.argv[4]

if (num1 === num2 && password === "hello world") {
   console.log("yay, you guessed the password")

}  else if (num1 > num2) {
   console.log("number 1 is greater")

} else if (num2 < num1) {
   console.log("number 2 is greater")

} else if (num1 === num2) {
    // tähän ei tarvitse tarkistusta, yhtäsuuruus on ainoa jäljellä oleva vaihtoehto.
   console.log("they are equal")

} else {
   console.log("what is happening, you should not be here and get this message") // and hence you should not include it in your code :)
}
