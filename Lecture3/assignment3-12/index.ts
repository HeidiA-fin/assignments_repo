const num1 = Number(process.argv[2])
const num2 = Number(process.argv[3])
const num3 = Number(process.argv[4])
const largest = Math.max(num1, num2, num3)
const minimum = Math.min(num1, num2, num3)
// Pystytkö tekemään tän ilman Math-kirjastoa?

console.log(largest, "is largest", minimum, "is smallest")

//check if equal numbers
if (num1 === num2 && num2 === num3 && num3 === num1) {
   console.log("they are equal")
}

console.log(num1, "is number1", num2, "is number2", num3, "is number3" )