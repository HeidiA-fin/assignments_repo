const arr = ['banaani', 'omena', 'mandariini', 'appelsiini', 'kurkku', 'tomaatti', 'peruna']
const sorted = arr.sort()
const pushed = arr.push("sipuli")
const removeF = arr.shift() //remove first item
// kaikki nämä metodit muokkaavat alkuperäist arraytä, joten uusia muuttujia ei tarvita niitä käyttäessä

console.log(arr[2], arr[4], arr.length)
console.log(sorted)
console.log(pushed) // tässä huomaat, että pushed on myös sortattu

//extras
console.log(removeF)
