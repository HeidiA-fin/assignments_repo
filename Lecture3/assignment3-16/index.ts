const choise = (process.argv[2])
const string = (process.argv[3])


if (choise === "lower") {
    console.log(string.toLowerCase())
} 

else if (choise === "upper") {
    console.log(string.toUpperCase())
}

// else kannattaa laittaa kiinni edelliseen if:iin, jotta se on selkeämpi lukea
// if (true) {
//     console.log('foo')
// } else if (true) {
//     console.log('bar')
// }