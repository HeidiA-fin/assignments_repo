//12-6 & 12-7
import server from "./server"

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})
