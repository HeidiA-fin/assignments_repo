import express, {Request, Response} from "express"
import { addProduct, readProductWithId, findAll, deleteProduct, updateProduct } from "./dao"

const router = express.Router()

//post product
router.post("/", async (req: Request, res: Response) => {
    const {id, name, price} = req.body
    await addProduct(name, price)
    res.send( {id, name, price} )
})

//search with id
router.get("/:id", async (req: Request, res: Response) => {
    const { id } = req.params
    await readProductWithId(parseInt(id, 10))
    res.send({ id })
})

//delete product
router.delete("/:id", async (req: Request, res: Response) => {
    const { id } = req.params
    await deleteProduct(parseInt(id, 10))
    res.send("Product deleted")
})

//update product
router.put("/:id", async (req: Request, res: Response) => {
    const { id } = req.params 
    const { name, price } = req.body 
    await updateProduct(parseInt(id, 10), name, price)
    res.send({id, name, price })
  })

//search all
router.get('/', async (req: Request, res: Response) => {
    const products = await findAll()
    res.send(products)
})

export default router