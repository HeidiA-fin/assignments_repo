//7-7

// Define conversion factors
const convertList = new Map<string, number>([
	["deciliter", 0.1],
	["liter", 1],
	["ounce", 0.0295735],
	["cup", 0.236588],
	["pint", 0.473176],
])

// Read command line arguments
const amount = parseFloat(process.argv[2])
const sourceUnit = process.argv[3]
const targetUnit = process.argv[4]

// Function to convert the units
export function convertUnits(amount: number, sourceUnit: string, targetUnit: string): number | null {
	if (!convertList.has(sourceUnit) || !convertList.has(targetUnit)) {
		return null
	}
  
	const sourceFactor = convertList.get(sourceUnit) as number
	const targetFactor = convertList.get(targetUnit) as number
	const convertedAmount = (amount * sourceFactor) / targetFactor
	return convertedAmount
}
  
// Convert the units
const convertedAmount = convertUnits(amount, sourceUnit, targetUnit)
if (convertedAmount !== null) {
	console.log(`Converted amount: ${convertedAmount.toFixed(2)} ${targetUnit}`)
}