import { convertUnits } from "../src/index.ts"

test("convert 1 liter to deciliter should be 10", () => {
	expect(convertUnits(1,"liter", "deciliter")).toBe(10)
})

test("convert 1 liter to ounce should be 33", () => {
	expect(convertUnits(1,"liter", "ounce")).toBe(33.81405650328842)
})

test("convert 1 liter to cup should be 4", () => {
	expect(convertUnits(1,"liter", "cup")).toBe(4.2267570629110525)
})

test("convert 1 liter to pint should be 2", () => {
	expect(convertUnits(1,"liter", "pint")).toBe(2.1133785314555262)
})

test("convert 10 deciliter to liter should be 1", () => {
	expect(convertUnits(10,"deciliter", "liter")).toBe(1)
})

test("invalid units provided", () => {
	expect(convertUnits(NaN,"something", "nope")).toBe(null)
})