import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    const min: number = Number(req.body?.min);
    const max: number = Number(req.body?.max);
    const returnInteger: boolean = Boolean(req.body?.returnInteger);
  
    if (isNaN(min) || isNaN(max)) {
      context.res = {
        status: 400,
        body: "Invalid minimum or maximum number provided.",
      };
      return;
    }
  
    if (returnInteger) {
      const randomNumber = Math.floor(Math.random() * (max - min)) + min;
      context.res = {
        body: randomNumber,
      };
    } else {
      const randomNumber = Math.random() * (max - min) + min;
      context.res = {
        body: randomNumber,
      };
    }
  };

export default httpTrigger;