//10-9
import express, { Request, Response } from "express"
import { errorHandler, logger } from "./middlewares"

const server = express()
server.use(express.json())

let notes = []

server.post('/notes', (req, res) => {
  const { id, title, content } = req.body
  const note = { id, title, content }

  notes.push(note)
  console.log("DEBUG: NOTE CREATED")

  res.status(201).json(note)
  console.log("DEBUG: NOTE INFORMATION:" , notes)
})

server.get('/notes', (req, res) => {
  res.json(notes)
})

server.delete('/notes/:id', (req, res) => {
	const id = req.params.id
 	console.log("DEBUG: TRYING TO DELETE NOTE PART 1")
  	const noteIndex = notes.findIndex((note) => note.id.toString() === id)

  if (noteIndex !== -1) {
    const deletedNote = notes.splice(noteIndex, 1)
	console.log("DEBUG: NOTE DELETED")
    res.json(deletedNote[0])
	
  } else {
    res.status(404).json({ error: 'Note not found' })
  }
})

server.use(errorHandler)

server.listen(3000, () => {
	console.log("Server running on port 3000")
})