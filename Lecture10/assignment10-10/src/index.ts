//10-10
import express, { Request, Response } from "express"
import { errorHandler, logger } from "./middlewares"

const server = express()
server.use(express.json())

const username = "admin"
const password = "password"

let notes = []

const authenticate = (req: Request, res: Response, next: any) => {
  const authHeader = req.headers["authorization"]
  if (authHeader) {
    const encodedCredentials = authHeader.split(" ")[1]
    const decodedCredentials = Buffer.from(encodedCredentials, "base64").toString("utf-8")
    const [authUsername, authPassword] = decodedCredentials.split(":")
    
    if (authUsername === username && authPassword === password) {
      return next()
    }
  }
  console.log("DEBUG: YOU ARE TRYING TO AUTHENTICATE HERE AND ITS NOT WORKING")
  res.status(401).json({ error: "Unauthorized" })
}

server.post("/notes", authenticate, (req, res) => {
  const { id, title, content } = req.body
  const note = { id, title, content }

  notes.push(note)
  console.log("DEBUG: NOTE CREATED")

  res.status(201).json(note)
  console.log("DEBUG: NOTE INFORMATION:", notes)
})

server.get("/notes", authenticate, (req, res) => {
  res.json(notes)
})

server.delete("/notes/:id", authenticate, (req, res) => {
  const id = req.params.id
  console.log("DEBUG: TRYING TO DELETE NOTE PART 1")
  const noteIndex = notes.findIndex((note) => note.id.toString() === id)

  if (noteIndex !== -1) {
    const deletedNote = notes.splice(noteIndex, 1)
    console.log("DEBUG: NOTE DELETED")
    res.json(deletedNote[0])
  } else {
    res.status(404).json({ error: "Note not found" })
  }
})

server.use(errorHandler)

server.listen(3000, () => {
  console.log("Server running on port 3000")
})