//8-2
import express, { Request, Response } from "express"

const server = express() // server is of type "Express"

server.listen(3000, () => {
	console.log("Listening to port 3000")
})

server.get("/", (_req: Request, res: Response) => {
	res.send("Hello from Docker")
})


