import express, { Request, Response} from "express"

export const router = express.Router()

const students: Array<{id: number,name: string, email: string}> = []

router.get("/students", (req: Request, res: Response) => {
	res.send(students.map((student) => student.id))
})

router.post("/student", (req: Request, res: Response) => {
	
	const student = req.body
	if (typeof student.id !== "number" || typeof student.name !== "string" || typeof student.email !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const studentInfo = {id: student.id, name: student.name, email: student.email}
		students.push(studentInfo)
		res.status(201).send()
	}
})


router.put("/student/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const { name, email } = req.body
	const student = students.find((item) => item.id === id)

	if (student === undefined) {
		res.status(404).send("ERROR, student id not found")

	} else if (!name && !email) {
		res.status(400).send("ERROR, name or email must be provided")

	} else {
		if (name) {
			student.name = name
		}
		if (email) {
			student.email = email
		}
		res.status(204).send()
	}
})

router.delete("/student/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const index = students.findIndex((item) => item.id === id)
  
	if (index === -1) {
		res.status(404).send("ERROR, student id not found")
	} else {
		students.splice(index, 1)
		res.status(204).send()
	}
})

router.get("/student/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const info = students.find(item => item.id === id)
	if (info === undefined) {
		res.status(404).send("ERROR, student id not found")
	} else {
		res.send(info)
	}
})

export default router