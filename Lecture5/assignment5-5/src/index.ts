//5-5

class IngredientObject { // Nimeämiskäytännöistä: koska tämä on objekti, sen nimeäminen objektiksi on redundanttia, olettaen, että meillä ei ole jotain muuta Ingredient-nimistä asiaa joka ei ole objekti
	name: string
	ammount : number
    
	constructor( name : string, ammount :number ) { 
		this.name = name
		this.ammount = ammount
	}

	scale = function ( factor: number){
		this.ammount = factor * this.ammount
	}
}

class Recipes { // Koska tämä objekti pitää sisällään yhden reseptin, konventio on että nimi on yksikössä
	rname : string // tämä on resepti-objektin sisällä oleva nimi, joten se voi huoletta olla vain "name", mikä vähentää typojen mahdollisuutta ja parantaa luettavuutta
	servings: number
	ingredient: Array<{name:string, ammount: number}>
        
	constructor( name: string, servings: number ,ingredient: Array<{name:string, ammount: number }>) { 
		this.rname = name
		this.servings = servings
		this.ingredient = ingredient
	}
	toString() {
		return this.ingredient.reduce((acc:string, cur: {name:string, ammount:number}) => {
			return acc + `- ${cur.name} (${cur.ammount})\n`
		}, `${this.rname} (${this.servings} servings)\n\n`)
	}
}

class HotRecipe extends Recipes {
	heatLevel:number

	constructor(rname: string, servings: number, ingredient: Array<{name:string, ammount: number }>, heatLevel: number) {
		super(rname, servings, ingredient)
		this.heatLevel = heatLevel
	}
}

const maito = new IngredientObject("maito", 7)
const mansikka = new IngredientObject("mansikka", 4)
const mustikka = new IngredientObject("mustikka", 8)
const jogurtti = new IngredientObject("jogurtti", 3)
const smoothie = new Recipes("smoothie", 6, [maito, mansikka, mustikka, jogurtti])


const hotSmoothie = new HotRecipe("tulinen smoothie", 4, [maito, mansikka, mustikka, jogurtti], 3) //HOW CAN I MAKE THIS WORK, PLS TELL ME?
// add one comma?


console.log(hotSmoothie.toString()) // IT WONT PRINT IT OUUUTTTTT
console.log(smoothie.toString())