//5-8
import fs from "fs"

//read from file and make it a variable
const content = JSON.parse(fs.readFileSync("./forecast_data.json", "utf-8"))

//change value of file
content.temperature = 10

//write new value to file
fs.writeFileSync("./forecast_data.json", JSON.stringify(content))