//5-14
const firstName = "Vichy"
const lastName = "Code"
const [username, password] = generator(firstName, lastName)

function generator(firstName: string, lastName: string): [string, string] {
	const currentYear = new Date().getFullYear().toString().slice(-2)
	const username = `B${currentYear}${firstName.slice(0, 2).toLowerCase()}${lastName.slice(0, 2).toLowerCase()}`
  
	const randomLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26))
	const randomSpecialChar = String.fromCharCode(33 + Math.floor(Math.random() * 15))
	const password = `${randomLetter}${firstName.charAt(0).toLowerCase()}${lastName.charAt(lastName.length - 1).toUpperCase()}${randomSpecialChar}${currentYear}`
  
	return [username, password]
}

// Tässä olisi voinut pienellä apufunktiolla selkeyttää tilannetta. Myös noita pitkiä konstruoituja stringejä on vaikea lukea, ehkä välivaiheet muuttujiin?
  
console.log(username, password)