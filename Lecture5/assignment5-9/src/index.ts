//5-9
function calculator(operator: string, num1: number, num2: number) {
	switch(operator) {

	case "+":
		return num1 + num2

	case "-":
		return num1 - num2

	case "*":
		return num1 * num2

	case "/":
		if (num2 === 0) {
			return "Cannot divide by zero" // JS/TS sallii muuten nollalla jakamisen. Tulos on Infinity, mikä on validi numero näissä kielissä.

		} else {
			return num1 / num2
		}

	default:
		return "Can't do that!"
	}
}

//change values here
console.log(calculator("*", 2, 9))