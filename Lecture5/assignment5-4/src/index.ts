//5-4

class IngredientObject {
	name: string
	ammount : number
    
	constructor( name : string, ammount :number ) { 
		this.name = name
		this.ammount = ammount
	}

	scale = function ( factor: number){ // syntaksi: scale(factor: number)
		this.ammount = factor * this.ammount
	}
}

class Recipes {
	rname : string
	servings: number
	ingredient: Array<{name:string, ammount: number}>
        
	constructor( name: string, servings: number ,ingredient: Array<{name:string, ammount: number }>) { 
		this.rname = name
		this.servings = servings
		this.ingredient = ingredient
	}
	toString() { // täällä oikea syntaksi
		return this.ingredient.reduce((acc:string, cur: {name:string, ammount:number}) => {
			return acc + `- ${cur.name} (${cur.ammount})\n`
		}, `${this.rname} (${this.servings} servings)\n\n`)
	}
}


const maito = new IngredientObject("maito", 7)
const mansikka = new IngredientObject("mansikka", 4)
const mustikka = new IngredientObject("mustikka", 8)
const jogurtti = new IngredientObject("jogurtti", 3)
const smoothie = new Recipes("smoothie", 6, [maito, mansikka, mustikka, jogurtti])
    
console.log(smoothie.toString())