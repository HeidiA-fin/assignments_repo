//5-12

//you can also determine this like 'const str = "this is a string"'
const str = process.argv[2]

function vowelCount(str: string): number {
	const vowels = ["a", "e", "i", "o", "u" , "y"]
	let count = 0

	for(let i = 0; i < str.length; i++ ) {
		if(vowels.includes(str[i])){
			count++
		}
	}
	return count
}

console.log(vowelCount(str))

// siisti ratkaisu