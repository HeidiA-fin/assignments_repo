//5-11
const numbers = [1, 5, 3, 9]

//calculate sum from array of numbers, average and above average numbers
function countAverage () {
	const sum = numbers.reduce((acc, num) => acc + num , 0)
	const average = sum / numbers.length
	const aboveAverageNumbers = numbers.filter(num => num > average)
	return aboveAverageNumbers
}

console.log(countAverage())

// siisti ratkaisu