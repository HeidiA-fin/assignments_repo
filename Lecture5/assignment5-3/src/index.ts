//5-3

//object constructor for ingredients
function IngredientsObjects(name: string, amount:number) {
	this.name = name
	this.amount = amount
}

//object constructor for recipe
function RecipeObjects(name: string, ingredients: Array<{name:string, amount:number}>, servings: number, ) {
	this.name = name
	this.ingredients = ingredients
	this.servings = servings
	
}
//object prototype for recipe
RecipeObjects.prototype.toString = function() {
	return this.ingredients.reduce((acc: string, cur: { name: string, amount: number }) => {
		return acc + `- ${cur.name} (${cur.amount})\n`
	}, `${this.name} (${this.servings} servings)\n\n`)
}
//object prototype for ingredients
IngredientsObjects.prototype.scale = function(factor: number) {
	this.amount = factor * this.amount
}

const flour = new IngredientsObjects("flour", 300)
const water = new IngredientsObjects("water", 150)
const oil = new IngredientsObjects("Oil", 30)
const salt = new IngredientsObjects("Salt", 0)

const tortillas = new RecipeObjects("tortillas", [flour, water, oil, salt], 12)

console.log(tortillas.toString())


