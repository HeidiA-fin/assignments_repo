//5-2

//object constructor for ingredients
function IngredientsObjects(name: string, amount:number) {
	this.name = name
	this.amount = amount
}

const maito = new IngredientsObjects("maito", 2)
const mansikka = new IngredientsObjects("mansikka", 4)

//object constructor for recipe
function RecipeObjects(name: string, ingredients: Array<{name:string, amount:number}>, servings: number, ) {
	this.name = name
	this.ingredients = ingredients
	this.servings = servings

    //toString?
}
const recipe = new RecipeObjects("testi", [maito, mansikka], 5)
console.log(recipe)


/* //5-2 works someway, my own way

//object constructor for ingredients
function IngredientsObjects(name: string, amount:number) {
	this.name = name
	this.amount = amount
}

const maito = new IngredientsObjects("maito", 2)
const mansikka = new IngredientsObjects("mansikka", 4)
const arr = [maito, mansikka]

//object constructor for recipe
function RecipeObjects(name: string, servings: number) {
	this.name = name
	this.ingredients = arr
	this.servings = servings
}
const recipe = new RecipeObjects("maito", 5)
console.log(recipe)*/

