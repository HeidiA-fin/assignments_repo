import express, { Request, Response } from "express"
import { findAll, findUserWithId, addNewUser, findAllPosts, findPostWithId, addNewPost, addNewComment, deleteComment, deletePost, deleteUser } from "./dao"

const router = express.Router()

//search all users and show id & username
router.get("/users", async (req: Request, res: Response) => {
    const users = await findAll()
    res.send(users)
})

//search user with id
router.get("/users/:id", async (req: Request, res: Response) => {
    const { id } = req.params
    const parseId = parseInt(id, 10)
    const users = await findUserWithId(parseId)
    res.send({ users })
})

//post new user
router.post("/users", async (req: Request, res: Response) => {
    const { usename, full_name, email } = req.body
    await addNewUser(usename, full_name, email)
    res.send({ usename, full_name, email })
})

//search all posts
router.get("/posts", async (req: Request, res: Response) => {
    const posts = await findAllPosts()
    res.send(posts)
})

//search posts with id
router.get("/posts/:id", async (req: Request, res: Response) => {
    const { id } = req.params
    await findPostWithId(parseInt(id, 10))
    res.send({ id })
})

//post new posts
router.post("/posts", async (req: Request, res: Response) => {
    const { title, content, post_date } = req.body
    await addNewPost(title, content, post_date)
    res.send({ title, content, post_date })
})

//search comment with id
router.get("/comment/:id", async (req: Request, res: Response) => {
    const { id } = req.params
    await findPostWithId(parseInt(id, 10))
    res.send({ id })
})

//post new comment
router.post("/comment", async (req: Request, res: Response) => {
    const { comment_post, comment_content, comment_date } = req.body
    await addNewComment(comment_post, comment_content, comment_date)
    res.send({ comment_post, comment_content, comment_date })
})

// delete user with id
router.delete("/users/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const parseId = parseInt(id, 10);
    await deleteUser(parseId);
    res.send(`User with id ${id} has been deleted.`);
});

// delete post with id
router.delete("/posts/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const parseId = parseInt(id, 10);
    await deletePost(parseId);
    res.send(`Post with id ${id} has been deleted.`);
});

// delete comment with id
router.delete("/comment/:id", async (req: Request, res: Response) => {
    const { id } = req.params;
    const parseId = parseInt(id, 10);
    await deleteComment(parseId);
    res.send(`Comment with id ${id} has been deleted.`);
});




export default router