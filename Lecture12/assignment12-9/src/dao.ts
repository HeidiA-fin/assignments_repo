import { executeQuery } from "./db"

//find all users
export const findAll = async (): Promise<any> => {
    const query = 'SELECT id_user, usename FROM users'
    const result = await executeQuery(query)
    const users = result.rows
    console.log("DEBUG: USERS \n", users)
    return users
}

//search user with id
export const findUserWithId = async (id: number) => {
    const query = 'SELECT id_user, usename, full_name, email FROM users WHERE id_user = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    const returnInfo = result.rows
    console.log('DEBUG: USER WITH ID \n', returnInfo)
    return returnInfo
}

//add new user
export const addNewUser = async (usename: string, full_name: string, email: string) => {
    const query = 'INSERT INTO users (usename, full_name, email) VALUES ($1, $2, $3) RETURNING id_user'
    const params = [usename, full_name, email]
    const result = await executeQuery(query, params)
    console.log("DEBUG: ADD NEW USER", result)
    return result
}

//find all posts
export const findAllPosts = async (): Promise<any> => {
    const query = 'SELECT id_user_post, post_title FROM posts'
    const result = await executeQuery(query)
    const posts = result.rows
    console.log("DEBUG: POSTS \n", posts)
    return posts
}

//search posts with id
export const findPostWithId = async (id: number) => {
    const query = 'SELECT posts.id_user_post, posts.post_title, posts.post_content, posts.post_date, comment.id_user_comment, comment.comment_post_title, comment.comment_content, comment.comment_date FROM posts JOIN comment ON posts.id_user_post = comment.id_user_comment;'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log('DEBUG: POSTS WITH ID AND COMMENT \n', result)
    return result
}

//add new post
export const addNewPost = async (title: string, content: string, post_date: number) => {
    const query = 'INSERT INTO posts (post_title, post_content, post_date) VALUES ($1, $2, NOW()) RETURNING id_user_post, post_content, post_date'
    const params = [title, content, post_date]
    const result = await executeQuery(query, params)
    console.log("DEBUG: ADD NEW POSTS", result)
    return result
}

//search comment with id
export const findCommentWithId = async (id: number) => {
    const query = 'SELECT posts.id_users_post, posts.title, posts.content,posts.post_date, comments.id_user_comment,comments.comment_post_title, comments.comment_content, comments.comment_date FROM posts LEFT JOIN comments ON posts.id_users_post = comments.id_user_comment WHERE posts.id_users_post = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log('DEBUG: POSTS WITH ID AND COMMENT \n', result)
    return result
}

//add new comment
export const addNewComment = async (comment_post_title: string, comment_content: string, comment_date: number) => {
    const query = 'INSERT INTO comment (comment_post_title, comment_content, comment_date) VALUES ($1, $2, NOW()) RETURNING comment_post_title, comment_content, comment_date'
    const params = [comment_post_title, comment_content, comment_date]
    const result = await executeQuery(query, params)
    console.log("DEBUG: ADD NEW COMMENT \n", result)
    return result
}

// delete user with id
export const deleteUser = async (id: number) => {
    const query = 'DELETE FROM users WHERE id_user = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log('DEBUG: DELETE USER \n', result)
    return result
}

//delete post with id
export const deletePost = async (id: number) => {
    const query = 'DELETE FROM posts WHERE id_user_post = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log('DEBUG: DELETE POST \n', result)
    return result
}

//delete comment with id
export const deleteComment = async (id: number) => {
    const query = 'DELETE FROM comment WHERE id_user_comment = $1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log('DEBUG: DELETE COMMENT \n', result)
    return result
}