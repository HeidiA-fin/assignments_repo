"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAll = exports.updateProduct = exports.deleteProduct = exports.readProductWithId = exports.addProduct = void 0;
const db_1 = require("./db");
const addProduct = (name, price) => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id';
    const params = [name, price];
    const result = yield (0, db_1.executeQuery)(query, params);
    console.log("DEBUG", result);
    return result;
});
exports.addProduct = addProduct;
const readProductWithId = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'SELECT id FROM products WHERE id = $1';
    const params = [id];
    const result = yield (0, db_1.executeQuery)(query, params);
    console.log('DEBUG \n', result);
    return result;
});
exports.readProductWithId = readProductWithId;
const deleteProduct = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'DELETE FROM products WHERE id = $1';
    const params = [id];
    const result = yield (0, db_1.executeQuery)(query, params);
    console.log('DEBUG AFTER DELETED \n', result);
    return result;
});
exports.deleteProduct = deleteProduct;
const updateProduct = (id, name, price) => __awaiter(void 0, void 0, void 0, function* () {
    const query = "UPDATE products SET name = $2, price = $3 WHERE id = $1";
    const params = [id, name, price];
    const result = yield (0, db_1.executeQuery)(query, params);
    console.log("Product updated", result);
    return result;
});
exports.updateProduct = updateProduct;
const findAll = () => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'SELECT * FROM products';
    const result = yield (0, db_1.executeQuery)(query);
    const products = result.rows;
    console.log("DEBUG \n", products);
    return products;
});
exports.findAll = findAll;
