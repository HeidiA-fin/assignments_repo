"use strict";
//12-6 & 12-7
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const router_1 = __importDefault(require("./router"));
const server = (0, express_1.default)();
server.use(express_1.default.json());
server.use(router_1.default);
(0, db_1.createProductsTable)();
const { PORT } = process.env;
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT);
});
