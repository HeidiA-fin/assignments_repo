"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dao_1 = require("./dao");
const router = express_1.default.Router();
//post product
router.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, name, price } = req.body;
    yield (0, dao_1.addProduct)(name, price);
    res.send({ id, name, price });
}));
//search with id
router.get("/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    yield (0, dao_1.readProductWithId)(parseInt(id, 10));
    res.send({ id });
}));
//delete product
router.delete("/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    yield (0, dao_1.deleteProduct)(parseInt(id, 10));
    res.send("Product deleted");
}));
//update product
router.put("/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { name, price } = req.body;
    yield (0, dao_1.updateProduct)(parseInt(id, 10), name, price);
    res.send({ id, name, price });
}));
//search all
router.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const products = yield (0, dao_1.findAll)();
    res.send(products);
}));
exports.default = router;
