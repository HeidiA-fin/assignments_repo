//11-6
import express, { Request, Response } from "express"
import { errorHandler, logger } from "./middlewares"

const server = express()
server.use(express.json())

interface Event {
  id: number
  title: string
  description: string
  date: Date
  time: string
}

let events: Event[] = []

server.get("/", (_req: Request, res: Response) => {
  res.json(events)
})

server.post("/", (req: Request, res: Response) => {
  const { id, title, description, date, time }: Event = req.body
  // ID:t kannattaa generoida backendissa, ja pyytää asiakkaalta vaan data. Tällai me kontrolloidaan id:n formaattia ja pidetään huolta, että ei tuu duplikaatteja.

  const parsedDate = new Date(date)
  if (isNaN(parsedDate.getTime())) {
    return res.status(400).json({ error: "Invalid date format" })
  }

  const event: Event = { id, title, description, date: parsedDate, time }

  events.push(event)
  console.log("DEBUG: EVENT CREATED")

  res.status(201).json(event)
  console.log("DEBUG: EVENT INFORMATION:", events)
})

server.put("/:eventId", (req: Request, res: Response) => {
  const { eventId } = req.params
  const { title, description, date, time }: Event = req.body

  const eventIndex = events.findIndex((event) => event.id.toString() === eventId) // käytä mielimmin .find kuin .findIndex

  if (eventIndex !== -1) {
    events[eventIndex] = {
      ...events[eventIndex], // tää destrukturointi jättää ainoastaan id:n, joka voitais eksplikoida (koska kaikki muut arvot asetetaan tossa alla)
      title: title || events[eventIndex].title,
      description: description || events[eventIndex].description,
      date: date || events[eventIndex].date,
      time: time || events[eventIndex].time,
    }

    console.log("DEBUG: EVENT MODIFIED")

    res.json(events[eventIndex])
  } else {
    res.status(404).json({ error: "Event not found" })
  }
})

server.delete("/:eventId", (req: Request, res: Response) => {
  const { eventId } = req.params

  const eventIndex = events.findIndex((event) => event.id.toString() === eventId)

  if (eventIndex !== -1) {
    const deletedEvent = events.splice(eventIndex, 1)
    console.log("DEBUG: EVENT DELETED")

    res.json(deletedEvent[0])
  } else {
    res.status(404).json({ error: "Event not found" })
  }
  // käytä mielummin .filter() joka ei mutatoi alkuperäistä listaa
})

server.get('/:monthNumber', (req: Request, res: Response) => {
  const monthNumber = parseInt(req.params.monthNumber, 10)
  console.log("DEBUG: TRY TO FIND MONTH HERE PART 1")

  const eventsInSpecifiedMonth = events.filter(event => {
    console.log("DEBUG: TRY TO FIND MONTH HERE PART 2")
    const eventDate = new Date(event.date) // event.date on jo tyyppiä Date, sitä ei tarvitse parsia enää
    const eventMonth = eventDate.getMonth()
    console.log("DEBUG: TRY TO FIND MONTH HERE PART 3")
    return eventMonth === monthNumber
  })

  console.log("DEBUG: TRY TO FIND MONTH HERE PART 4")
  res.json(eventsInSpecifiedMonth)
})

server.use(errorHandler)

server.listen(3000, () => {
  console.log("Server running on port 3000")
})