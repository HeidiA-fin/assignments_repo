import { useState } from 'react'
import './App.css'

function App() {
  const [balls, setBalls] = useState<Array<number>>([])

  const clickHandler = () => {

    const rand = Math.floor(Math.random() * (100-1))

    const findSame = balls.find(num => num === rand)
    if(findSame) return

    setBalls((balls)=> [...balls, rand])
  }

const renderBalls = balls.map(ball => <div className="ball" key={ball}>{ball}</div>)


  return (
      <div className='app'>
        {renderBalls}
        <div className='buttonsWrap'>
        <button onClick={clickHandler}>Add</button>
        <button onClick={() => setBalls([])}>Reset</button>
        </div>
      </div>
  )
}

export default App