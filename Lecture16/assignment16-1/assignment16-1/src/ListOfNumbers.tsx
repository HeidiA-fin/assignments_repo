import { useState, ChangeEvent } from "react"


const numbers = [
    { number: 1234567890 },
    { number: 8763973654 }
]



function AddPhoneNumber() {

    const [phoneNumber, setPhoneNumber] = useState("")

    const checkDigits = (value: string) => {
        setPhoneNumber(value)

        console.log("hello does this work", value)
    }

    const [phones, setPhones] = useState<string>("")
    const [numberList, setNumberList] = useState<string[]>([])
    const [tenDigits, setTenDigits] = useState("")

    return (
        <div className='phoneNumberList'>
            <input type="number"
                name="phone"
                placeholder="Add a new number"
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                    checkDigits(event.target.value);
                }} />

            <div className='thisIsListing'>
                {numbers.map(number => {
                    return <div>
                        {number.number}
                    </div>
                })}
            </div>
        </div>
    )
}

export default AddPhoneNumber