import CatFinder from "./CatFinder"

function App() {

  return (
    <div className='App'>
      <CatFinder />
    </div>
  )
}

export default App