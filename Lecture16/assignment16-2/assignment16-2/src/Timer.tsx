import { useState, useEffect } from "react";

const Timer = () => {
    const [seconds, setSeconds] = useState(0)

    useEffect(() => {
        const timer = setTimeout(() => {
            setSeconds((prevSeconds) => prevSeconds +1)
        }, 1000)
        
        return () => {
            clearTimeout(timer)
        }
    }, [seconds])

    return (
        <div>
            <h2>Timer: {seconds}</h2>
        </div>
    )
}

export default Timer