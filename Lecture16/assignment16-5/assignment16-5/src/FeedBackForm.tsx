import { SetStateAction, useState } from "react"

const FormComponent = () => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [selectedOption, setSelectedOption] = useState("")
    const [textarea, setTextarea] = useState("")

    const handleOptionChange = (event: { target: { value: SetStateAction<string>; }; }) => {
        setSelectedOption(event.target.value)
    }

    const submitHandler = (event: { preventDefault: () => void; }) => {
        event.preventDefault()
        console.log(name, email, selectedOption, textarea)
    }

    const resetForm = () => {
        setName("")
        setEmail("")
        setSelectedOption("")
        setTextarea("") 
    }

    return (
        <form className="form-container" onSubmit={submitHandler}>
            <label htmlFor="name">
                Feedback:
            </label>
            <input
                type="radio"
                value="feedback"
                checked={selectedOption === "feedback"}
                onChange={handleOptionChange}

            />
            <br></br>
            <label htmlFor="name">
                Suggestion:
            </label>
            <input
                type="radio"
                value="suggestion"
                checked={selectedOption === "suggestion"}
                onChange={handleOptionChange}
            />
            <br></br>
            <label htmlFor="name">
                Question:
            </label>
            <input
                type="radio"
                value="question"
                checked={selectedOption === "question"}
                onChange={handleOptionChange}
            />
            <br></br>

            <textarea placeholder="type here"
                value={textarea}
                onChange={(event) => setTextarea(event.target.value)}>

            </textarea>

            <br></br>
            <input
                type="text"
                placeholder="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
            />
            <br></br>
            <input
                type="text"
                placeholder="name"
                value={name}
                onChange={(event) => setName(event.target.value)}
            />

            <input
            type="submit" />
            
            <button onClick={resetForm}>Reset</button>
        </form>
        
    )
};

export default FormComponent