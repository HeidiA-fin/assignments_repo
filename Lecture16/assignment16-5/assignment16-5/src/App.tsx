import FeedBackForm from "./FeedBackForm"
import "./FeedBackForm.css"

function App() {

  return (
    <div className='App'>
      <FeedBackForm />
    </div>
  )
}

export default App