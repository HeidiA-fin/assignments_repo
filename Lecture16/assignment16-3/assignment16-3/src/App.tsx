import DadJokes from "./DadJokes"

function App() {

  return (
    <div className='App'>
      Dad Jokes
      <DadJokes />
    </div>
  )
}

export default App
