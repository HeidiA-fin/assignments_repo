import { useState, useEffect } from "react"
import axios from "axios"

const FetchDadJokes = () => {
    const [joke, setJoke] = useState("")

    useEffect(() => {
        async function fetchData() {
            const url = 'https://api.api-ninjas.com/v1/dadjokes'
            const config = {
                headers: {
                    'x-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
                }
            }
            const data = await axios.get(url, config)
            console.log("DEBUG", data)
            setJoke(data.data[0].joke);

        }
        fetchData()
    }, [])

    return <div>{joke}</div>
};

export default FetchDadJokes
