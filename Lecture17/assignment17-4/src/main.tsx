import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider, useLoaderData } from 'react-router-dom'

const listing = [
  { name: "nimi1", phone: "1234511", email: "maili1", id: "1" },
  { name: "nimi2", phone: "1234522", email: "maili2", id: "2" },
  { name: "nimi3", phone: "1234533", email: "maili3", id: "3" },
]

function loader({ params }: any) {
  return params.contact
}

const MainPage = () => {
  return <div className='mainpage'>These are the Awesome Adventures of React Router MAIN PAGE</div>
}

const ContactPage = () => {
  const params = useLoaderData() as string
  const contact = listing.find((item) => item.id === params)

  if (!contact) {
    return <div className='contactpage'>Wrong id or something went horribly wrong</div>
  }

  const { name, phone, email, id } = contact

  return (
    <div className='contactpage'>
      <h2>Contact information is:</h2>
      <div>Name: {name}</div>
      <div>Phone: {phone}</div>
      <div>Email: {email}</div>
      <div>Id: {id}</div>
    </div>
  )
}

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainPage />
  },
  {
    path: '/:contact',
    element: <ContactPage />,
    loader: loader
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)