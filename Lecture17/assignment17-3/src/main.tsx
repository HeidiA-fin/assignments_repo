import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

const MainPage = () => {
  return <div className='mainpage'>These are the Awesome Adventures of React Router MAIN PAGE</div>
}

const ContactPage = () => {
  return <div className='contactpage'>Here you can find Awesome Adventures of React Router CONTACT PAGE</div>
}

const router = createBrowserRouter ([
  {
    path: '/',
    element: <MainPage />
  },

  {
    path: '/contact',
    element: <ContactPage />
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
