import { useLoaderData } from "react-router-dom"

export const listing = [
    { name: "nimi1", phone: "1234511", email: "maili1", id: "1" },
    { name: "nimi2", phone: "1234522", email: "maili2", id: "2" },
    { name: "nimi3", phone: "1234533", email: "maili3", id: "3" },
]

export function loader({ params }: any) {
    console.log("DEBUG: on loader", params)
    return params.contact
}

export const ContactPage = () => {
    const params = useLoaderData() as string
    console.log("DEBUG: on contactpage: ", params)
    const contact = listing.find((item) => item.id === params)
    console.log("DEBUG: on contactpage", params, contact)

    if (!contact) {
        return <div className='contactpage'>Wrong id or something went horribly wrong</div>
    }

    const { name, phone, email, id } = contact

    return (
        <div className='contactpage'>
            <h2>Contact information is:</h2>
            <div>Name: {name}</div>
            <div>Phone: {phone}</div>
            <div>Email: {email}</div>
            <div>Id: {id}</div>
        </div>
    )
}

export default ContactPage