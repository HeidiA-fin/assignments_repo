import { createBrowserRouter } from "react-router-dom"
import ContactPage, { loader } from "./ContactPage"

export const MainPage = () => {
    return <div className='mainpage'>These are the Awesome Adventures of React Router MAIN PAGE</div>
  }

  console.log("DEBUG: after mainpage() on browserrouter.tsx")

export const router = createBrowserRouter([
    {
      path: '/',
      element: <MainPage />,
    },
    
    {
      path: '/contact',
      element: <ContactPage />,
      loader: loader
    }
  ])
console.log("DEBUG: after router() on browserroutes.tsx", ContactPage)
export default router