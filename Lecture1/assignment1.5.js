const playerCount = 4
const isStressed = true
const hasIcecream = true
const sun = true
const rain = false
const temperature = 20
const seeSuzy = true
const seeDan = true
const seeingDay = "tuesday night"

if (playerCount === 4) {
    console.log("you can play game")
} else
    console.log("you cant play game")
    
if (!isStressed || hasIcecream) {
    console.log("mark is happy")
} else
    console.log("mark is not happy")

if (sun === true && rain === false && temperature >= 20) {
    console.log("it is beach day")
} else 
    console.log("it is not beach day")

if ((seeSuzy || seeDan) && !(seeSuzy && seeDan) && seeingDay === "tuesday night") {
    console.log("arin is happy")
} else 
    console.log("arin is not happy")