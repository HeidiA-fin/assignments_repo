let price = 20
let discount = 5
let dPercentage = discount / price  * 100
let dPrice = price - discount

console.log(`Original price ${price} €, discount percentage ${dPercentage} %, discounted price ${dPrice} €`)

// Tehtävänannossa discount on discount percentage, joten on nyt epäselvää, mikä dPercentage on