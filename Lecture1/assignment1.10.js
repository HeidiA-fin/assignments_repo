let lastName = "Radeon"
let age = 26
let isDoctor = true
let sender = "Shrek"

if (isDoctor) {
    console.log(`Dear Dr ${lastName}`)

    switch (age+1) {
        case 1:
            console.log(`Congratulations on your ${age +1}st! Many happy returns!`);
            break;
        case 2:
            console.log(`Congratulations on your ${age +1}nd! Many happy returns!`);
            break;
        case 3:
            console.log(`Congratulations on your ${age +1}rd! Many happy returns!`);
            break;
        default:
            console.log(`Congratulations on your ${age +1}th! Many happy returns!`);
            break;
    }
}

else {
    console.log(`Dear Mx ${lastName}`)
    switch (age+1) {
        case 1:
            console.log(`Congratulations on your ${age +1}st! Many happy returns!`);
            break;
        case 2:
            console.log(`Congratulations on your ${age +1}nd! Many happy returns!`);
            break;
        case 3:
            console.log(`Congratulations on your ${age +1}rd! Many happy returns!`);
            break;
        default:
            console.log(`Congratulations on your ${age +1}th! Many happy returns!`);
            break;
    }
}

console.log(`Sincerely ${sender}`);

// aika monta kertaa sama teksti koodissa. Muuttujaa käyttämällä olisi vältytty toistolta. Tässä on myös käytännössä kaksi kertaa sama koodiblokki.

// Ehdotan, että koitat tehdä tämän uudestaan ilman, että koodissa on samaa asiaa toistettu useampaan kertaan.

