const number1 = 3
const number2 = 5

const sum = number1 + number2
const fraction = number1 - number2
const product = number1 * number2
const exponent = number1 ** number2
const modulo = number1 % number2
const average = sum / 2

console.log(sum, fraction, product, exponent, modulo, average)