let days = 365
let hours = 24
let seconds = 3600
let secondsInYear = days * hours * seconds

console.log(`There is ${secondsInYear} seconds in a year`)