const text = "this is a test";
let number = 2;
const trueFalse = true;

console.log(text, typeof text)
console.log(number, typeof number)
console.log(trueFalse, typeof trueFalse)

number = 5;

console.log(text, typeof text)
console.log(number, typeof number)
console.log(trueFalse, typeof trueFalse)

//this is a comment

// käytä puolipisteitä systemaattisesti, joko joka rivillä tai ei ollenkaan