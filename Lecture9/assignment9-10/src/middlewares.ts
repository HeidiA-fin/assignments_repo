import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'

interface CustomRequest extends Request {
  user?: any;
  isAdmin?: boolean;
}

export const isUserAdmin = (req: CustomRequest, res: Response, next: NextFunction) => {
  console.log(req.user.isAdmin)
  if(req.user.isAdmin === undefined){
      console.log("this is admin status: ", req.isAdmin)
      if (!req.isAdmin) {
          return res.status(403).json({ error: "Forbidden" })
      }
  }
  next()
}

export const authenticate = async (req: CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get("Authorization")
  if (!auth?.startsWith("Bearer ")) {
      return res.status(401).send("Missing token")
  }
  const token = auth.substring(7)
  const secret = process.env.SECRET ?? ""
  if(secret === ""){
      res.status(404).send("Secret is missing")
  }
  try {
      const decodedToken = await jwt.verify(token, secret)
      req.user = decodedToken as string
      next()
  } catch (error) {
      return res.status(401).send("Invalid token")
  }
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
  console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`)
  next()
}

export const validateBookData = (req: Request, res: Response, next: NextFunction) => {
  const { id, name, author, read } = req.body

  if (!id || !name || !author || read === undefined) {
    return res.status(400).send('Invalid book data')
  }

  next()
}
// Käyttäjän autentikointi middleware puuttuu kokonaan

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err)
  res.status(500).send('Internal Server Error')
}