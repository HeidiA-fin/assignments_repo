//9-2
import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import { router } from "./studentRouter"

const server = express()
server.use(express.json())
server.use(express.static("public"))
server.use(router)
server.use(logger)
server.use(unknownEndpoint)

server.listen(3002, () => {
	console.log("Listening to port 3000")
})
