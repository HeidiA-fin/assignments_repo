import express, { Request, Response } from "express"
import argon2 from "argon2"

export const userRouter = express.Router()

const userStorage: Array<{ username: string; password: string }> = []

userRouter.post("/register", (req: Request, res: Response) => {
  const user = req.body
  console.log(user.username, user.password)

  const password = user.password
  argon2
    .hash(password)
    .then((hashedPassword) => {
      console.log("HASHED", hashedPassword)
      userStorage.push({ username: user.username, password: hashedPassword })
      console.log("DEBUG", userStorage)
      res.sendStatus(201)
    })
    .catch((error) => {
      console.error("Error hashing password:", error)
      res.sendStatus(500)
    })
})

userRouter.post("/login", async (req: Request, res: Response) => {
  const { username, password } = req.body

  const user = userStorage.find((user) => user.username === username)
  if (!user) {
    res.sendStatus(401)
    return
  }

  try {
    const passwordMatch = await argon2.verify(user.password, password)
    if (passwordMatch) {
      res.sendStatus(204)
    } else {
      res.sendStatus(401) 
    }
  } catch (error) {
    console.error("Error verifying password:", error)
    res.sendStatus(500)
  }
})