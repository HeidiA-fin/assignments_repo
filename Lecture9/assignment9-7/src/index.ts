//9-7
import "dotenv/config"
import jwt from "jsonwebtoken"

const payload = { username: "sugarplumfairy" }
const secret = process.env.SECRET
const options = { expiresIn: "1h"}

const token = jwt.sign(payload, secret, options)
console.log(token)
const secretToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFsaWVuIiwiaWF0IjoxNjg1MzYyNTA1fQ.hvUjv3NqD1Gf-QiU5_LD8GdXjENa2vAg2omSxT7B--c"

const result = jwt.verify(secretToken, secret)
console.log(result)