import express, { Request, Response} from "express"
import argon2 from "argon2"

export const userRouter = express.Router()

const userStorage: Array<{username: string, password: string}> = []

userRouter.post("/register", (req: Request, res: Response) => {
	const user = req.body
	console.log(user.username, user.password)

	const password = user.password
	argon2.hash(password)
		.then(result => console.log("HASHED", result))

	userStorage.push(user.username, user.password)
	res.status(201).send()
	console.log("DEBUG", userStorage)

})
