import express from "express"
import "dotenv/config"
import { logger, unknownEndpoint } from "./middlewares"
import { router } from "./studentRouter"
import userRouter from "./userRouter"

const server = express()
server.use(express.json())
server.use(router)
server.use(userRouter)
server.use(logger)
server.use(unknownEndpoint)

server.listen(3002, () => {
	console.log("Listening to port 3002")
})

export default server