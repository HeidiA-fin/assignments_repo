//9-9
import server from './server'

const port = 3002
server.listen(port, () => {
    console.log('Server listening on port', port)
})