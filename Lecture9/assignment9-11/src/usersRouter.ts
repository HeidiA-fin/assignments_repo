import express, { Request, Response, NextFunction } from 'express'
import argon2 from 'argon2'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()

interface User {
    username: string,
    password: string,
    isAdmin?: boolean
}

let users: Array<User> = []

const secret = process.env.SECRET

export const userRouter = express.Router()

userRouter.post('/register', async (req: Request, res: Response) => {
    const {username, password} = req.body
    const existingUser = users.find(user => user.username === username)

    if (existingUser !== undefined) {
        return res.status(400).send("Registration failed: Username already exists!")
    }
    if (username === process.env.ADMIN_USER) {
       return res.status(400).send("Username is invalid!")
    }

    const hash = await argon2.hash(password)

    const newUser: User = {
        username, 
        password: hash
        }

    users.push(newUser)
    console.log("User registered:", newUser)
    
    const token = jwt.sign( {username}, process.env.SECRET || "", {expiresIn: "3h"} )
    res.send({token})
    }
)

userRouter.post("/login", async (req: Request, res: Response) => {
    const { username, password } = req.body
    console.log("are you here to log in?")
     const user = users.find(user => user.username === username)
    
    if (username === process.env.ADMIN_USER) {
        const adminHash = process.env.ADMIN_HASH ?? ""
        const isValidAdminPassword = await argon2.verify(adminHash, password)

        if (!isValidAdminPassword) {
            return res.status(401).send("Incorrect username or password 1")
        }

        const token = jwt.sign( {username, isAdmin: true}, secret, {expiresIn: "3h"} )
        console.log("You have logged in")
        return res.send({token})
    }

    if (user === undefined) {
        return res.status(401).send("Incorrect username or password 2")
    }

    const isValidPassword = await argon2.verify(user.password, password)

    if (!isValidPassword) {
        return res.status(401).send("Incorrect username or password 3")
    }

    argon2
    .verify(user.password, password)
    .then((passwordMatches) => {

        if(passwordMatches) {

            const token = jwt.sign({username}, secret, {expiresIn: "3h"})
            return res.status(200).json({token})
        } else {
            return res.status(401).send("Incorrect username or password")
        }
    })
    .catch((error) => {
        console.error("Error during password or verification", error)
        return res.status(500).send()
    })
})

export default userRouter
