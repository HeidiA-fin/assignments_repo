import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()

interface CustomRequest extends Request {
    username?: string;
    isAdmin?: boolean;
}

export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get("Authorization")

  if (!auth?.startsWith("Bearer ")) {
    return res.status(401).send("Invalid token 1")
  }

  const token = auth.substring(7)
  const secret = process.env.SECRET ?? ""

  try {
    const decodedToken = jwt.verify(token, secret)
    if (typeof decodedToken === "string") {
      return res.status(401).send("Invalid token 2")
    }

    req.username = decodedToken.username
    req.isAdmin = decodedToken.isAdmin
    next()

  } catch (error) {
    return res.status(401).send("Invalid token 3")
 }
}

export const isAdmin = (req: CustomRequest, res: Response, next: NextFunction) => {
  if(req.isAdmin) {
    next()
  } else {
    res.status(401).send("Missing admin priviledges")
  }
}

export const validateBookData = (req: Request, res: Response, next: NextFunction) => {
  const { id, name, author, read } = req.body

  if (!id || !name || !author || read === undefined) {
    return res.status(400).send('Invalid book data')
  }

  next()
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
  console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`)
  next()
}

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err)
  res.status(500).send('Internal Server Error')
}