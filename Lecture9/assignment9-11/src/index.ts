//9-11
//PÄÄSIN REKISTERÖIMÄÄN ADMININ, LOGGAAMAAN EHKÄ SISÄÄN (EI TOIMINUT OIKEIN), EN SAANUT LISÄTTYÄ KIRJOJA KOSKA UNAUTHORIZED
import express from 'express'
import helmet from 'helmet'
import { logger, errorHandler } from './middlewares'
import booksRouter from './booksRouter'
import usersRouter from './usersRouter'

const server = express()
server.use(express.json())
server.use(helmet())
server.use(logger)

server.use('/api/v1/users', usersRouter)

server.use('/api/v1/books', booksRouter)

server.use(errorHandler)

server.listen(3000, () => {
  console.log('Server running on port 3000')
})