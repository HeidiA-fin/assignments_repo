//4-21
const input = Number(process.argv[2])
let isPrime = true

if (input === 1) {
	console.log("Number 1 can not be prime or composite number")
}
else if (input > 1) {
	for (let i = 2; i < input; i++) {
		if (input % i === 0) {
			isPrime = false
            // tähän voi vielä lisätä breakin, koska turha tätä looppia on loppuun jatkaa, jos ollaan jo tulos löydetty.
		}
	}

	if (isPrime) {
		console.log(`${input} is a prime number` )
	} else {
		console.log(`${input} is not a prime number`)
	}
}

// Katso korjaukset läpi ja pistä kyssäreitä tuleen jos on mitään epäselvyyttä! 