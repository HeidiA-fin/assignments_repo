//1 named function
function plus (num1: number, num2: number) {
	return num1 + num2
}

const sum = plus(7, 2)
console.log(sum)

//2 anonymous function
const plus1 = function (num2: number, num3: number) {
	return num2 + num3
}

const sum1 = plus1(5, 9)
console.log(sum1)

//3 arrow function
const arrowPlus = (num4: number, num5: number) => num4 + num5
console.log(arrowPlus(1, 3))

// Miten näitä pitäisi muuttaa, että ne toimisivat myös kolmella parametrilla?