//4-18
const str = (process.argv[2])

function checkPalindrome (str: string) {
	const strCheck = str.split("").reverse().join("")

	if (str === strCheck) {
		console.log("This is palindrome")
	}
	else {
		console.log("This is not palindrome")
	}
}

checkPalindrome(str)