//4-12
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]

const divisible = arr.filter((divide) => divide % 3 === 0)
console.log(divisible)

const multiply = arr.map(arr => arr * 2)
console.log(multiply)

const sum = arr.reduce((acc, cur) => acc + cur, 0)
console.log(sum)