const pls = (yes: number):number => { // pls ja yes?
	if (yes < 7) { // ehtona on yes < 7, joten ohjelma menee nyt tähän haaraan joka kerta, myös esim arvolla -8, joten se ei lopu koskaan jos alkuarvona on pienempi kuin 7
		return yes*pls(yes-1)
	} else {
		return yes
	}
}

console.log(pls(4))