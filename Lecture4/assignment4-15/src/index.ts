//4-15
const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"]
const ordinals = ["st", "nd", "rd", "th"]

for (let i = 0; i < competitors.length; i++) {
	const contestant = competitors[i]
	if(i > 3){
		const place = ordinals[3]
		console.log((i + 1) + place , "competitor was " + contestant )
	}
	else{
		const place = ordinals[i]
		console.log((i + 1) + place , "competitor was " + contestant )
	}

    // molemmissa haaroissa on aika samanlaista koodia. vielä voisi toistoa vähentää tallentamalla ordinalsin indeksin omaan muuttujaansa.
}