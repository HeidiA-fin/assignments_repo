function epicTitleLogger(part1 = "this is a test") {
	const words = part1.split("") // .split("") jakaa jokaisen kirjaimen välistä, .split(" ") jakaa sanojen välistä
	let finalproduct = ""
	for (let i = 0; i < words.length; i++) {
		const word = words[i]
		finalproduct = finalproduct + word[0].toUpperCase() + word.substring(1) + " "
	}
	return finalproduct.trim()
}

console.log(epicTitleLogger())
