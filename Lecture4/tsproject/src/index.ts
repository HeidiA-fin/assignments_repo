function myToUpperCase(oneParameter: string) {
	const words = oneParameter.split(" ")
	let finalProduct = ""
	for (let i = 0; i < words.length; i++) {
		const word = words[i]
		finalProduct = finalProduct + word[0].toUpperCase() + word.substring(1) + " "
	}
	console.log(finalProduct)
}
console.log(myToUpperCase)