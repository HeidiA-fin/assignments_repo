const joinStrings = (strArr: string[], separator: string) => strArr.reduce((acc, cur) => acc + cur + separator, " ")

const stringList = ["testi1", "testi2", "testi3"]

console.log(joinStrings(stringList, ", "))

// nyt tulee vielä ylimääräinen separaattori loppuun, miten saisimme ne vain väleihin?