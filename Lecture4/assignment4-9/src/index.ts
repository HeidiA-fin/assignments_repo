const numbers = [
	749385,
	498654,
	234534,
	345467,
	956876,
	365457,
	235667,
	464534,
	346436,
	873453
]

const checklist = numbers.filter((divide) => divide % 3 === 0 || divide % 5 === 0)
console.log(checklist)