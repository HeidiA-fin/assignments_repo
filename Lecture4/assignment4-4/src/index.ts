const dice = () => {
	const sides = (number: number) => {
		const random = Math.floor(Math.random() * number) + 1
		return random
	}
    // "sides is declared but never read": palautat dice:n, eli funktion itsensä

	return dice
}

const diceSix = sides(6) // "cannot find sides": sides funktio on tuolla dice funktion sisällä, joten se ei näy tähän kohtaan koodia
const diceEight = sides(8)

const damage = diceSix() + diceSix() + diceEight() + diceEight()

console.log(dice())