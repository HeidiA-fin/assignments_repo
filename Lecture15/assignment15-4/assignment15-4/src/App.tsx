import Instrument from "./Instrument"
import violin from "./violin.png"
import guitar from "./guitar.png"
import tuba from "./tuba.png"
import "./app.css"

function App() {

  return (
    <div className='App'>
      <h1>Component with Props!</h1>
      <Instrument name={"Violin"} price={100} image={violin} />
      <Instrument name={"Guitar"} price={50} image={guitar} />
      <Instrument name={"Tuba"} price={300} image={tuba} />
    </div>
  )
}

export default App