interface Props {
    name: string
    price: number
    image: string
}


function Instrument (props: Props) {
    let string = `Instrument name is ${props.name} and price is ${props.price}`

    return (
        <div className="Instrument">
        <img src={props.image} />
        {string} 
        </div>
    )
}
export default Instrument
