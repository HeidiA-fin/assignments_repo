import { useState } from 'react'

function App() {
  
  
  const [count1, setCount1] = useState(0)
  const [count2, setCount2] = useState(0)
  const [count3, setCount3] = useState(0)
  const sum = `Sum of your clicks is ${count1 + count2 + count3}`

  return (
    <>
    <h1> Three Buttons With UseState</h1>
      <div className="buttonOne">
        <button onClick={() => setCount1((count1) => count1 + 1)}>
          Button1 clicks:  {count1}
        </button>
      </div>

      <br></br>

      <div className='buttonTwo'>
        <button onClick={() => setCount2((count2) => count2 + 1)}>
          Button2 clicks: {count2}
        </button>
      </div>

      <br></br>

      <div className='buttonThree'>
        <button onClick={() => setCount3((count3) => count3 + 1)}>
          Button3 clicks: {count3}
        </button>
      </div>

      <br></br>

      <div className='results'>
        {sum}
      </div>

    </>
  )
}

export default App
