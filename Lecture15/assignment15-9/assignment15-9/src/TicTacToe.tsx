import { useState } from "react";
import "./TicTacToe.css"

type Player = "X" | "O";
type Board = Player | null;

const initialBoard: Board[] = Array(9).fill(null);

const calculateWinner = (board: Board[]): Player | null => {
  const lines = [
    // Rows
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    // Columns
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    // Diagonals
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (const line of lines) {
    const [a, b, c] = line;
    if (board[a] && board[a] === board[b] && board[a] === board[c]) {
      return board[a] as Player;
    }
  }

  return null;
};

const TicTacToe = () => {
  const [board, setBoard] = useState<Board[]>(initialBoard);
  const [currentPlayer, setCurrentPlayer] = useState<Player>("X");
  const [winner, setWinner] = useState<Player | null>(null);

  const handleClick = (index: number) => {
    if (board[index] || winner) {
      return;
    }

    const updatedBoard = [...board];
    updatedBoard[index] = currentPlayer;
    setBoard(updatedBoard);

    const newWinner = calculateWinner(updatedBoard);
    if (newWinner) {
      setWinner(newWinner);
    } else {
      setCurrentPlayer(currentPlayer === "X" ? "O" : "X");
    }
  };

  const handleReset = () => {
    setBoard(initialBoard);
    setCurrentPlayer("X");
    setWinner(null);
  };

  const renderBoard = () => {
    return (
      <div className="board">
        {[0, 1, 2].map((row) => (
          <div className="row" key={row}>
            {[0, 1, 2].map((col) => {
              const index = row * 3 + col;
              return (
                <button
                  className="square"
                  key={index}
                  onClick={() => handleClick(index)}
                >
                  {board[index]}
                </button>
              );
            })}
          </div>
        ))}
      </div>
    );
  };

  let status;
  if (winner) {
    status = `Player ${winner} has won!`;
  } else if (board.every((square) => square !== null)) {
    status = "It's a tie!";
  } else {
    status = `Current Player: ${currentPlayer}`;
  }

  return (
    <div className="game">
      <h2>Tic Tac Toe</h2>
      {renderBoard()}
      <div className="status">{status}</div>
      <button className="reset-button" onClick={handleReset}>
        Reset
      </button>
    </div>
  );
};

export default TicTacToe;
