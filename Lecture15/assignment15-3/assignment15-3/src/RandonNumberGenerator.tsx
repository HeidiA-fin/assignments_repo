const RandomNumberGenerator = () => {
    return (
        <div className='RandomNumberGenerator'>
            <h1>Your random number is: {Math.floor(Math.random() * (100-1))}</h1>
        </div>

    )
}

export default RandomNumberGenerator