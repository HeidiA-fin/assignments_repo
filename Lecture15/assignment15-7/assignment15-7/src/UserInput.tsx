import { useState, ChangeEvent} from 'react'

function UserInput() {
    const [original, setOriginal] = useState('')
    const [update, setUpdate] = useState(original)
    
    const onTextChange = (event: ChangeEvent<HTMLInputElement>) => {
        setOriginal(event.target.value)
    }

    const handleClick = () => {
        setUpdate(original)
    }

    return (
        <div className='App'>

            <h2>HERE IS YOUR SUBMITTED MESSAGE: {update}</h2>

            <input value={original} onChange={onTextChange} /> 

            <button onClick={handleClick}>Submit</button>
        </div>
    )
}

export default UserInput
