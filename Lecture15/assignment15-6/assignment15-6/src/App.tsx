import ListOfNames from "./ListOfNames"
import "./App.css"

function App() {

  return (
    <div className='App'>
      <h2>Alternating List of Names</h2>
      <ListOfNames />
    </div>
  )
}

export default App
