function ListOfNames() {

    /* cheap way out :) */
    const people = [
        { name: <b>Ari</b>},
        { name: <i>Jari</i>},
        { name: <b>Kari</b>},
        { name: <i>Sari</i>},
        { name: <b>Mari</b>},
        { name: <i>Sakari</i>},
        { name: <b>Jouko</b>},
    ]

    return (
        <div className='ListOfNames'>
            {people.map(person => {
                return <div>
                {person.name}
                </div>
            })}
        </div>
    )
}

export default ListOfNames