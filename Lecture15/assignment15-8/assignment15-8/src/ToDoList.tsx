import { useState, ChangeEvent } from 'react'

function ToDoList() {
    const [todo, setTodo] = useState('')
    const [todoList, setTodoList] = useState<string[]>([])

    const addTodo = () => {
        if (todo !== "") {
            setTodoList([...todoList, todo])
            setTodo("")
        }
    }

    const toggleTodo = (index: number) => {
        const updatedList = [...todoList]
        updatedList[index] = updatedList[index].startsWith("Done: ")
            ? updatedList[index].replace("Done: ", "")
            : `Done: ${updatedList[index]}`
        setTodoList(updatedList)
    }

    const removeTodo = (index: number) => {
        const updatedList = [...todoList]
        updatedList.splice(index, 1)
        setTodoList(updatedList)
    }

    return (
        <div className='ToDoList'>

            <h2>To Do List: </h2>
            <h3>Click on task to marked it done</h3>

            <input type="text"
                name="todo"
                value={todo}
                placeholder="Add a new task"
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                    setTodo(event.target.value);
                }} />

            <button className="add-button" onClick={addTodo}>Add to list</button>

            <ul className="todoList">
                {todoList.map((todo, index) => (
                    <div className='todo' key={index}>
                        {/*click on task to make it done or undone*/}
                        <li className={todo.startsWith("Done: ") ? "done" : ""}
                            onClick={() => toggleTodo(index)}>
                            {todo}
                        </li>
                        <button className='remove-button' onClick={() => removeTodo(index)}>Remove task</button>
                    </div>
                ))}
            </ul>
        </div>
    )
}

export default ToDoList