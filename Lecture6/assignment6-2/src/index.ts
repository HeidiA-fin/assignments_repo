//6-2
new Promise<void>((resolve, _reject) => { //_reject ilmoittaa, että parametri on mutta sitä ei käytetä
	setTimeout(() => {
		console.log("3")
		resolve()
	}, 1000)
})

	.then(() => { 
		return new Promise<void>((resolve, _reject) => {
			setTimeout(() => {
				console.log("2")
				resolve()
			}, 1000)
		})})

	.then(() => { 
		return new Promise<void>((resolve, _reject) => {
			setTimeout(() => {
				console.log("1")
				resolve()
			}, 1000)
		})})

	.then(() => { 
		return new Promise<void>((resolve, _reject) => {
			setTimeout(() => {
				console.log("GO")
				resolve()
			}, 1000)
		})})
