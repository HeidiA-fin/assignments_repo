//6-6
const arr = [5, 77, 32, -66, 832]

function findMaxNumber (arr: number[]): number {
	let max = arr[0]
	for (let i = 1; i < arr.length; i++ ) {
		if (arr[i] > max) {
			max = arr[i]
		}
	}
	return max
}

function findSecondLargestNumber (array: number[]): number {
	let max = array[0]
	let secondLargest = array[1]

	if (secondLargest > max) {
		[max, secondLargest] = [secondLargest, max]
	}

	for (let i = 2; i < array.length; i++) {
		if (array[i] > max) {
			secondLargest = max
			max = array[i]
		} else if (array[i] > secondLargest && array[i] < max) {
			secondLargest = array[i]
		}
	}

	return secondLargest
}

console.log(findMaxNumber(arr))
console.log(findSecondLargestNumber(arr))