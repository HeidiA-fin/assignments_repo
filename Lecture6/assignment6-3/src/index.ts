//6-3
const getValue = function (): Promise<number> {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(Math.random())
		}, Math.random() * 1500)
	})
}

const getValueOne = () => {
	getValue().then((randomNumber) => {
		a = randomNumber // Alustamaton muuttuja a, ei mene kääntäjästä läpi 
		return getValue()
	})
		.catch((reject) => {
			console.log("What are you doing")
		})
}

const getValueOneHere = async () => {
	const value1 = await getValue()
	const value2 = await getValue()
	console.log(`${value1} ${value2}`)
}

getValueOneHere()
getValueOne()