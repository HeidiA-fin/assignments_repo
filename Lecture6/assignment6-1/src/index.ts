//6-1
const callBackCountdown = function (arg:number, func: () => void): void {
	setTimeout(() => {
		func()
	}, arg)
}
// Hyvää funktion käyttöä!
callBackCountdown(1000,() => {
	console.log("3")
	callBackCountdown(1000, () => {
		console.log("2")
		callBackCountdown(1000, () => {
			console.log("1")
			callBackCountdown(1000, () => {
				console.log("GO")
			})
		})
	})
})
