//6-8
import axios from "axios"

interface Todo {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
}

interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}

async function fetchTodos(): Promise<Todo[]> {
	const response = await axios.get("https://jsonplaceholder.typicode.com/todos/")
	return response.data
}

async function fetchUsers(): Promise<User[]> {
	const response = await axios.get("https://jsonplaceholder.typicode.com/users/")
	return response.data
}

async function fetchData() {
	try {
		const [todos, users] = await Promise.all([fetchTodos(), fetchUsers()])

		// Task 2: Map users to an object with userId as the key
		const usersMap: { [userId: number]: User } = {}
		for (const user of users) {
			usersMap[user.id] = user
		}

		// Task 3 & 4: Modify todos with user data and reduce user field
		const modifiedTodos = todos.map((todo) => {
			const { userId, ...rest } = todo
			const user = usersMap[userId]
			const { name, username, email } = user
			return { ...rest, user: { name, username, email } }
		})

		console.log(modifiedTodos)
	} catch (error) {
		console.error("Error:", error.message)
	}
}

// Call the function to fetch and process the data
fetchData()