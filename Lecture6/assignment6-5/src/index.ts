//6-5
import axios from "axios"

interface Response {
    Title: string
    Released: number
    Genre: string
}

async function movie (title: string, year?: number) {
	const url = `http://www.omdbapi.com/?apikey=c3a0092f&t=${title}&y=${year}`
	const response = await axios.get(url)
	const typedResult: Response = response.data
	console.log(typedResult)
}

movie("titanic")
// Nice!