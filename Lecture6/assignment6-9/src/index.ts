//6-9
function delay(ms: number): Promise<void> {
	return new Promise(resolve => setTimeout(resolve, ms))
}
  
function raceLap(): Promise<number> {
	return new Promise((resolve, reject) => {
		const crashChance = Math.random() * 100
		if (crashChance <= 3) {
			reject("Crashed!")
		} else {
			const lapTime = Math.floor(Math.random() * 6) + 20
			resolve(lapTime)
		}
	})
}
  
async function race(drivers: string[], laps: number): Promise<{ winner: string; stats: Record<string, any> }> {
	const stats: Record<string, any> = {}
	let winner: string | undefined
  
	for (const driver of drivers) {
		stats[driver] = {
			totalTime: 0,
			bestLapTime: Infinity,
		}
	}
  
	for (let lap = 1; lap <= laps; lap++) {
		console.log(`\nLap ${lap} starts:`)
  
		const lapPromises = drivers.map(async (driver) => {
			try {
				const lapTime = await raceLap()
				console.log(`${driver} completed lap ${lap} in ${lapTime} seconds`)
  
				stats[driver].totalTime += lapTime
				if (lapTime < stats[driver].bestLapTime) {
					stats[driver].bestLapTime = lapTime
				}
			} catch (error) {
				console.log(`${driver} crashed on lap ${lap}`)
				stats[driver].crashed = true
			}
		})
  
		await Promise.all(lapPromises)
		await delay(1000)
	}
  
	for (const driver of drivers) {
		if (!stats[driver].crashed && (!winner || stats[driver].totalTime < stats[winner].totalTime)) {
			winner = driver
		}
	}
  
	return { winner: winner!, stats } //eslint valittaa winner!:sta
}
  
const drivers = ["Driver 1", "Driver 2", "Driver 3"]
const laps = 5
  
race(drivers, laps)
	.then((result) => {
		console.log("\nRace finished!")
		console.log(`Winner: ${result.winner}`)
		console.log("Stats:", result.stats)
	})
	.catch((error) => {
		console.error("Error:", error)
	})
// Hyvät rallit!